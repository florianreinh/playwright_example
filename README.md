The example code to my Playwright short tutorial:

https://www.florianreinhard.de/2020-04-10/playwright-for-browser-automation/

# Requirements
NodeJS Version >= 10

# Installation
git clone git@bitbucket.org:florianreinh/playwright_example.git

cd playwright_example

npm install

# Usage
node playwright_test.js