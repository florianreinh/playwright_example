const {firefox} = require("playwright");

(async () => {
  const browser = await firefox.launch({headless: false, slowMo: 50});
  const context = await browser.newContext();

  const page = await context.newPage();
  await page.goto("https://www.amazon.com");
  await page.fill("#twotabsearchtextbox", "Red Shoes");

  const searchBox = await page.$("#twotabsearchtextbox");
  await searchBox.press("Enter");

  await page.waitFor("img[data-image-latency='s-product-image']");
  await page.screenshot({path: "./screenshot.jpg", type: "jpeg", fullPage: true});
  await browser.close();
})();
